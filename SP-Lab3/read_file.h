/*
 * File read module header file
 *
 * Change Logs:
 * Date           Author           Notes
 * 2020-11-17     Xiyang.Zhang     first version
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
* Read the data to store the array and return the array address.
* parameter file_name: File path and file name
* parameter num: number of elements.
* parameter size: array capacity.
*/
float *read_file(char *file_name, int *num, int *size);
