﻿# C - based basicstats

# Project structure

 - Main  : program entry
	 - basicstats.c
 - Readfile : File read module
	 - read_file.h
	 - read_file.c
 - Calculate : 
	 - statistical.h
	 - statistical.c

# Development environment
Developers are advised to use the following environment to avoid problems with the release
 - Ubantu 20.04 LTS
 - GCC version 9.3.0

# Compiling
 - gcc -c basicstats.c -o basicstats.o
 - gcc -c read_file.c -o read_file.o
 - gcc -c statistical.c -o statistical.o

# Link
 - gcc basicstats.o read_file.o statistical.o -o basicstats -lm
 
# Usage
  ./basicstats small.txt     # Pass a data file to the program

