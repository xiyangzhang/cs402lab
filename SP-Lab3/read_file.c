/*
 * File read module implementation. 
 *
 * Change Logs:
 * Date           Author           Notes
 * 2020-11-17     Xiyang.Zhang     first version
 */


#include "read_file.h"


/*
* Read the data to store the array and return the array address.
* parameter file_name: File path and file name
* parameter num: number of elements.
* parameter size: array capacity.
* return : the array address.
*/
float *read_file(char *file_name, int *num, int *size){
	FILE *fp;
	if ((fp =fopen(file_name, "r")) == NULL){
		printf("Fail to open Data file!\n");
		exit(0);
	}
	float *cur_arr = (float *)malloc(*size * sizeof(float)); // Apply for array space.
	
	while(!feof(fp)){	
		fscanf(fp, "%f\n", (cur_arr + (*num)));
		*num = *num + 1;
		if(*num == *size) {   // Filled array.
			*size *= 2;       // Expand the array capacity to 2 times.
			float *new_arr = (float *)malloc(*size * sizeof(float));
			memcpy(new_arr, cur_arr, *num * sizeof(float));    // The original array element moves to the new array.
			free(cur_arr);      // Let's release the array space
			cur_arr = new_arr;
		}
	}
	fclose(fp);
	return cur_arr;
}
