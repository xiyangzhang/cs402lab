/*
 * main module of Program
 *
 * Change Logs:
 * Date           Author           Notes
 * 2020-11-17     Xiyang.Zhang     first version
 */

#include <stdlib.h>

#include "read_file.h"
#include "statistical_calc.h"

/*
* function is to show the results.
* parameter num: quantity of data.
* parameter mean: mean of data.
* parameter median: median of data.
* parameter std: standard deviation of data.
* parameter size: size of array.
*/
void show_result(int num,double mean,double median,double std,int size);


/*
* Program entry function
*/
int main(int argc, char *argv[]){
	// Judgment parameter number
    if(argc<2){
        printf("Pass filename to read from ...\n");
        return 0;
    }

    int size=20;  // size of array.
	int num=0;    // number of elements.
    char * file_name = argv[1];
    
	// Read the data into the array
	float *arr = read_file(file_name,&num, &size);

    // Statistical indicators for calculating data
    double mean = get_mean(arr, num);
    double std = get_sd(arr, num);
    sort(arr, num);
    double median = get_median(arr, num);

	free(arr);
    show_result(num,mean,median,std,size);
};


void show_result(int num,double mean,double median,double std,int size){
	printf("Results:\n");
	printf("--------\n");
	printf("Num values: %15d\n",num);
	printf("      mean: %15.3f\n",mean);
	printf("    median: %15.3f\n",median);
	printf("    stddev: %15.3f\n",std);
	printf("Unused array capacity:   %d\n",(size - num));
};
