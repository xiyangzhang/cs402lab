/*
 * index calculation module implementation. 
 *
 * Change Logs:
 * Date           Author           Notes
 * 2020-11-17     Xiyang.Zhang     first version
 */

#include "statistical_calc.h"

/*
* function to get mean of values;
* parameter arr:  data array.
* parameter num:  number of data.
* return : mean of data.
*/
double get_mean(float *arr, int num){
    double sum=0;
    for(int i=0; i<num; i++){
         sum += arr[i];
    }
    double mean = sum / num;
    return mean;
};


/*
* function to get standard deviation of values;
* parameter arr:  data array.
* parameter num:  number of data.
* return : standard deviation of data.
*/
double get_sd(float *arr, int num){
    //sqrt((sum((xi - mean)^2))/N);
    double std = 0;
    double mean = get_mean(arr, num);
    for(int i=0; i<num; i++){
         std += pow(arr[i] - mean, 2);
	}
	return sqrt(std/num);
};

/*
* function to get median of values;
* parameter arr:  data array.
* parameter num:  number of data.
* return : median of data.
*/
double get_median(float *arr, int num){
    if(num%2 == 1)
		return (arr[num/2]);
    else {
        double min = arr[num/2-1];
        double max = arr[num/2];
        return ((min + max)/2);
    }
};


// ------------------ Sort Functions ---------------------
/*
* The sorting algorithm is selected according to the amount of data.
* parameter arr:  data array.
* parameter num:  number of data.
*/
void sort(float *arr, int num){
	if ( num <= 100){
		select_sort(arr, num);       // Small amount of data using selective sorting algorithm.
	}else {
		quick_sort(arr, 0, num-1);   // Large amount of data using quick sort algorithm.
	}
};

/*
* selective sorting algorithm.
* parameter arr:  data array.
* parameter num:  number of data.
*/
float select_sort(float *arr, int num){
    int i, j, min_idx;
    for(i=0; i<num-1; i++){           
        min_idx = i;
        for(j=i+1; j<num; j++)
            if(arr[j] < arr[min_idx])
                min_idx = j; 
        swap(&arr[min_idx], &arr[i]);
    }
};

/*
* quick sort algorithm.
* parameter arr:  data array.
* parameter num:  number of data.
*/
void quick_sort(float *arr, int first, int end){
	if(first < end){
		int mid = partition(arr, first, end);
		quick_sort(arr, first, mid-1);
	    quick_sort(arr, mid+1, end);
	}
};

/*
* function of swap element values.
* parameter xp: The memory address of the first element.
* parameter yp: The memory address of the second element.
*/
void swap(float *xp, float *yp){
    float temp = *xp;
    *xp = *yp;
    *yp = temp;
};

/*
* parameter arr:  data array.
* parameter first: Start index with flowered partition.
* parameter end:  End index with flowered partition.
*
*/
int partition(float *arr, int first, int end){
    // Reference point optimization, take the middle of the three values
	int mid = (first+end) / 2;
    int swap_index = 0;
    if((arr[first] - arr[mid])*(arr[mid] - arr[end]) >= 0){
        swap_index = mid;
    } else if((arr[mid] - arr[first])*(arr[first] - arr[end]) >= 0){
        swap_index = first;
    } else {
        swap_index = end;
    }
    swap(&arr[swap_index], &arr[end]);
    
    int pre = first - 1;
    while(first != end){
        if (arr[first] < arr[end]){
            pre = pre + 1;
            swap(&arr[pre], &arr[first]);
        }
        first = first + 1;
	}
    swap(&arr[pre+1],&arr[end]);
    return pre+1;
};
