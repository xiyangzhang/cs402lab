/*
 * Display module header file
 *
 * Change Logs:
 * Date           Author           Notes
 * 2020-11-03     Xiyang.Zhang     first version
 */

#include "readfile.h"

// System directory page
void menu();

// Display employee data
void displayee(struct Employee ee);

// Display employee list data
void displayList(struct Employee *db, int num);
