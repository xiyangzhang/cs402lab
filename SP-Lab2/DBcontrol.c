/*
 * DB_control module implementation. 
 *
 * Change Logs:
 * Date           Author           Notes
 * 2020-11-03     Xiyang.Zhang     first version
 */
 
#include "DBcontrol.h"

void init_DB(char *file){
	FILE *fp;
    num=0;
    max_id=99999;
    
	printf("System initialization, load the database file...\n");
	fp = open_file(file, "r");
	while(!feof(fp)){
		struct Employee ee;
		fscanf(fp,"%d %s %s %d\n",&ee.six_digit_ID,ee.first_name,ee.last_name,&ee.salary);
		if(ee.six_digit_ID!=0 && ee.first_name!=NULL && ee.last_name != NULL && ee.salary != 0){
		EeDB[num] = ee;			
		} 
		if (ee.six_digit_ID>=100000 && ee.six_digit_ID<=999999 && ee.six_digit_ID>max_id){
			max_id=ee.six_digit_ID;
		}
		num++;
	}
	close_file(fp);
	sort_DB_by_ID();
	printf("Initialization is complete. Welcome to use.\n\n");
};

// Data files in use are usually ordered, 
// so insertion sort can be used to achieve efficiency close to O(n).
int sort_DB_by_ID(){
	int i,j;
	struct Employee temp;
	for(i=2; i<=num; i++){
		temp = EeDB[i];
		j = i - 1;
		while(temp.six_digit_ID<EeDB[j].six_digit_ID){
			EeDB[j+1] = EeDB[j];
			j--;
		}
		EeDB[j+1] = temp;
	}
	max_id=EeDB[num].six_digit_ID;
	return 0;			
};

int add_ee(void){
	printf("Start adding employees.\n");
	char first_name[MAXNAME];
	char last_name[MAXNAME];
	int salary;
	int flag;
	while(1){
		printf("Please enter the employee's first name: ");
		flag = read_string(first_name);
		if (flag == 0){
			break;
		} else {
			printf("Incorrect input. Please re-enter\n");
		}
	}
	while(1){
		printf("Please enter the employee's last name: ");
		flag = read_string(last_name);
		if (flag == 0){
			break;
		} else {
			printf("Incorrect input. Please re-enter\n");
		}		
	}
	while(1){
		printf("Please enter the employee's salary: ");
		flag = read_int(&salary);
		if (flag == 0 && salary>=30000 && salary<=150000){
			break;
		} else {
			printf("Incorrect input. Please re-enter\n");
		}		
	}
	struct Employee ee;
	ee.six_digit_ID = max_id + 1;
	strcpy(ee.first_name,first_name);
	strcpy(ee.last_name,last_name);
	ee.salary = salary;
	displayList(&ee, 0);
	printf("Confirm the new employee[1:yes/0:no]:");
	read_int(&flag);
	if (flag == 1){
		EeDB[++num]=ee;
		max_id++;
	}
	printf("\n");
};

int binary_search(int target){
    int low = 0;
	int high = num;
	int mid;
	int flag = 0;
	while(high>=low){
		mid=(high+low)/2;
		if(EeDB[mid].six_digit_ID>target){
			high=mid-1;
		} else if (EeDB[mid].six_digit_ID<target){
			low=mid+1;
		} else {
		 	flag = 1;
		 	break;
		}
	}
	if (flag == 1){
		return mid;
	} else {
		return -1;
	}		
};

void search_by_id(){
	printf("Please enter the employee's id: ");
	int key_id;
	int flag;
	while(1){
		flag = read_int(&key_id);
		if (flag == 0 && key_id>=100000 && key_id<=999999){
			break;
		} else {
			printf("Incorrect input. Please re-enter\n");
		}
	}

	int findindex = binary_search(key_id);
	if(findindex != -1){
		displayList(&EeDB[findindex], 0);
		printf("\n");	
	 	return;
	};
	printf("There is no employee of the surname.\n");
	printf("\n");
	return;	
};

void search_by_last_name(){
	char lstnm[MAXNAME];
	int flag;
	while(1){
		printf("Please enter the employee last name: ");
		flag = read_string(lstnm);
		if (flag == 0){
			break;
		} else {
			printf("Incorrect input. Please re-enter\n");
		}	
	}
	int i;
	for(i=0; i<=num; i++){
		if(strcasecmp(lstnm,EeDB[i].last_name)==0){
			displayList(&EeDB[i], 0);
			printf("\n");
		 	return;
		}
	}
	printf("There is no employee of the surname.\n");
	printf("\n");
	return;	
};

void display(){
	displayList(EeDB, num);
	printf("\n");
};

void save_DB(char *file){
	printf("Saving data, exit when finished...\n");
	FILE *fp = open_file(file, "w");
	if(fp==NULL){
		printf("File cannot open! \n");
		return;
	}
	int i;
	for(i=0; i<=num; i++){
		if(EeDB[i].six_digit_ID >=100000 && EeDB[i].six_digit_ID <=999999){
			fprintf(fp,"%d %s %s %d\n",EeDB[i].six_digit_ID,EeDB[i].first_name,EeDB[i].last_name,EeDB[i].salary);
		}
	}
	close_file(fp);
	printf("goodbye!\n\n");
};

void del_by_id(){
	printf("Please enter the employee's id: ");
	int key_id;
	int flag;
	while(1){
		flag = read_int(&key_id);
		if (flag == 0 && key_id>=100000 && key_id<=999999){
			break;
		} else {
			printf("Incorrect input. Please re-enter\n");
		}
	}

	int findindex = binary_search(key_id);
	if(findindex == -1){
		printf("There is no employee of the surname.\n");
		printf("\n");
		return;	
	} else {
		displayList(&EeDB[findindex], 0);
		printf("Do you want to delete the employee information[1:yes/0:no]\n");
		read_int(&flag);
		if (flag == 1) {
			if (EeDB[findindex].six_digit_ID == max_id){
				max_id = EeDB[findindex-1].six_digit_ID;
			} 
			int i;
			for (i=findindex; i<num; i++) {
				EeDB[i] = EeDB[i+1]; 
			}
			num--;
		} else {
			printf("Undelete.\n");
			printf("\n");	
		 	return;
		} 
		printf("Employee information deletion completion.\n");
		printf("\n");	
	 	return;
	};

};

void update_by_id(){
	printf("Please enter the employee's id: ");
	int key_id;
	int flag;
	while(1){
		flag = read_int(&key_id);
		if (flag == 0 && key_id>=100000 && key_id<=999999){
			break;
		} else {
			printf("Incorrect input(100000<=id<=999999). Please re-enter\n");
		}
	}

	int findindex = binary_search(key_id);
	if(findindex == -1){
		printf("There is no employee of the surname.\n");
		printf("\n");
		return;	
	} else {
		displayList(&EeDB[findindex], 0);	
		// Get update information.
		char first_name[MAXNAME];
		char last_name[MAXNAME];
		int id;
		int salary;
		
		while(1){
			printf("Please enter the employee's new id: ");
			flag = read_int(&id);
			if (flag == 0 && key_id>=100000 && key_id<=999999){
				flag=binary_search(id);
				if(flag==-1){
					break;	
				}else {
					printf("Id already exists, please re-enter.\n");
					continue;
				}
			} else {
				printf("Incorrect input(100000<=id<=999999). Please re-enter\n");
			}
		}				
		while(1){
			printf("Please enter the employee's new first name: ");
			flag = read_string(first_name);
			if (flag == 0){
				break;
			} else {
				printf("Incorrect input. Please re-enter\n");
			}
		}
		while(1){
			printf("Please enter the employee's last name: ");
			flag = read_string(last_name);
			if (flag == 0){
				break;
			} else {
				printf("Incorrect input. Please re-enter\n");
			}		
		}
		while(1){
			printf("Please enter the employee's salary: ");
			flag = read_int(&salary);
			if (flag == 0 && salary>=30000 && salary<=150000){
				break;
			} else {
				printf("Incorrect input. Please re-enter\n");
			}		
		}
		EeDB[findindex].six_digit_ID=id;
		strcpy(EeDB[findindex].first_name,first_name);
		strcpy(EeDB[findindex].last_name,last_name);
		EeDB[findindex].salary = salary;
		displayList(&EeDB[findindex], 0);
		sort_DB_by_ID();
		printf("Employee information update completed.\n");
		printf("\n");
		return;
	}
};

void topm(){
	printf("Please enter the Top M: ");
	int Topm;
	int flag;
	while(1){
		flag = read_int(&Topm);
		if (flag == 0 && Topm>=1 && Topm<=num){
			break;
		} else {
			printf("Incorrect input(0<M<Number(employees)). Please re-enter\n");
		}
	}
	struct Employee heap[Topm]; 
	int i;
	for(i=1; i<=Topm; i++){
		struct Employee copy=EeDB[i];
		heap[i]=copy;
	}
	build_min_heap(heap, Topm);
	for(i=Topm+1; i<=num; i++){
		if(EeDB[i].salary>heap[1].salary){
			heap[1]=EeDB[i];
			min_heapify(heap,1);
		}
	}
	heap_sort(heap);
	displayList(heap, Topm);
	printf("\n");
};

void search_all_by_last_name(){
	char lstnm[MAXNAME];
	int flag;
	while(1){
		printf("Please enter the employee last name: ");
		flag = read_string(lstnm);
		if (flag == 0){
			break;
		} else {
			printf("Incorrect input. Please re-enter\n");
		}	
	}
	int i;
	int total = 0;
	struct Employee tsln[MAXARR+1];
	for(i=0; i<=num; i++){
		if(strcasecmp(lstnm,EeDB[i].last_name)==0){
			struct Employee copy=EeDB[i];
			tsln[++total]=copy;
		}
	}
	if(total==0){
		printf("There is no employee of the surname.\n");
		printf("\n");
		return;	
	}else {
		displayList(tsln, total);
		printf("\n");
	 	return;
	} 
};



void swap(struct Employee *heap, int i, int j){
	struct Employee tmp = heap[i];
	heap[i] = heap[j];
	heap[j] = tmp;	
};

int parent(int i){
	return i >> 1;
};

int left(int i){
	return i << 1;
};

int right(int i){
	return i<<1|1;
};

void min_heapify(struct Employee *heap, int i){
	int l = left(i);
	int r = right(i);
	int smallest;
	if (l<=size && heap[l].salary<heap[i].salary){
		smallest=l;
	}else{
		smallest=i;
	}
	if (r<=size && heap[r].salary<heap[smallest].salary){
		smallest=r;
	}	
	if (smallest!=i){
		swap(heap, i, smallest);
		min_heapify(heap, smallest);
	}		
};

void build_min_heap(struct Employee *heap, int num){
	int i;
	size=num;
	for(i=size/2; i>0; i--){
		 min_heapify(heap,i);
	}
};

void heap_sort(struct Employee *heap){
	int i;
	for(i=size; i>1; i--){
		swap(heap,1,i);
		size--;
		min_heapify(heap, 1);
	}
}


