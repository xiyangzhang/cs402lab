/*
 * DB_control module header file
 *
 * Change Logs:
 * Date           Author           Notes
 * 2020-11-03     Xiyang.Zhang     first version
 */

#include "displaypage.h"

// Defining an array of employees.
struct Employee EeDB[MAXARR];  
// Define the maximum employee ID.
int max_id;
// Define the number of employees.
int num;

// Initializes the database with a data file.
void init_DB(char *file);

// Sort the employee array by ID.
int sort_DB_by_ID();

// Add an employee.
int add_ee(void);

// Binary search for employees by ID.
void search_by_id();

// Search for employees by last name.
void search_by_last_name();

// Display database data.
void display();

// Save the data back to the data file.
void save_DB(char *file);
