/*
 * File read/write module header file
 *
 * Change Logs:
 * Date           Author           Notes
 * 2020-11-03     Xiyang.Zhang     first version
 */
 
 
#include <stdlib.h>
#include <stdio.h>
#include <string.h> 

#define MAXNAME 64
#define MAXARR 1024

struct Employee
{
	int six_digit_ID;
	char first_name[MAXNAME];
	char last_name[MAXNAME];
	int salary;
};

/*
* Opens the file according to the mode and returns the handle
*  parameter filename :   File path and file name
*  parameter sc : Open mode "r, w, a..."
*  return: The FILE pointer
*/
FILE *open_file(char *filename, char *sc);

// Read an int from the console and assign to "path"
int read_int(int *path);

// Read a string from the console and assign to "path"
int read_string(char *path);

// Read a float from the console and assign to "path"
int read_float(float *path);

// Close the file flow according to the file handle
void close_file(FILE *f);
