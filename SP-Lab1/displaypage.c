/*
 * Display module implementation. 
 *
 * Change Logs:
 * Date           Author           Notes
 * 2020-11-03     Xiyang.Zhang     first version
 */

#include "displaypage.h"



void menu()
{
	printf("  ------------------------------------------------\n\n");
	printf("  *    Employee information management system    *\n\n");
	printf("  ----------    System Function menu    ----------\n");
	printf("  ------------------------------------------------\n");
	printf("       * 1.Print the Database             \n");
	printf("       * 2.Lookup employee by ID          \n");
	printf("       * 3.Lookup employee by last name   \n");
	printf("       * 4.Add an Employee                \n");
	printf("       * 5.Quit                           \n");
	printf("  ------------------------------------------------\n");
	printf("\n");
	printf(" Enter your choice: ");
};

void displayee(struct Employee ee)
{
	printf("|    %-8d |  %-10s |  %-10s |    $%-7d |\n",ee.six_digit_ID,ee.first_name,ee.last_name,ee.salary);
	printf("---------------------------------------------------------\n");
};

void displayList(struct Employee *db, int num)
{
	printf("---------------------------------------------------------\n");
	printf("| employee_ID |  first_name |  last_name  |    salary   |\n");
	printf("---------------------------------------------------------\n");
	if (num == 1){
		displayee(db[0]);
	} else {
		int i=1;
		while(i<=num){
			displayee(db[i++]);
		}
	}
	printf("\n");
	printf("Number of Employees (%d)\n\n", num);
};



