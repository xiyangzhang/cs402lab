/*
 * DB_control module implementation. 
 *
 * Change Logs:
 * Date           Author           Notes
 * 2020-11-03     Xiyang.Zhang     first version
 */

#include "DBcontrol.h"

void init_DB(char *file)
{
	FILE *fp;
    num=0;
    max_id=99999;
    
	printf("System initialization, load the database file...\n");
	fp = open_file(file, "r");
	while(!feof(fp)){
		struct Employee ee;
		fscanf(fp,"%d %s %s %d\n",&ee.six_digit_ID,ee.first_name,ee.last_name,&ee.salary);
		if(ee.six_digit_ID!=0 && ee.first_name!=NULL && ee.last_name != NULL && ee.salary != 0){
		EeDB[num] = ee;			
		} 
		if (ee.six_digit_ID>=100000 && ee.six_digit_ID<=999999 && ee.six_digit_ID>max_id){
			max_id=ee.six_digit_ID;
		}
		num++;
	}
	close_file(fp);
	sort_DB_by_ID();
	printf("Initialization is complete. Welcome to use.\n\n");
};

// Data files in use are usually ordered, 
// so insertion sort can be used to achieve efficiency close to O(n).
int sort_DB_by_ID()
{
	int i,j;
	struct Employee temp;
	for(i=2; i<=num; i++){
		temp = EeDB[i];
		j = i - 1;
		while(temp.six_digit_ID<EeDB[j].six_digit_ID){
			EeDB[j+1] = EeDB[j];
			j--;
		}
		EeDB[j+1] = temp;
	}
	return 0;			
};

int add_ee(void)
{
	printf("Start adding employees.\n");
	char first_name[MAXNAME];
	char last_name[MAXNAME];
	int salary;
	int flag;
	while(1){
		printf("Please enter the employee's first name: ");
		flag = read_string(first_name);
		if (flag == 0){
			break;
		} else {
			printf("Incorrect input. Please re-enter\n");
		}
	}
	while(1){
		printf("Please enter the employee's last name: ");
		flag = read_string(last_name);
		if (flag == 0){
			break;
		} else {
			printf("Incorrect input. Please re-enter\n");
		}		
	}
	while(1){
		printf("Please enter the employee's salary: ");
		flag = read_int(&salary);
		if (flag == 0 && salary>=30000 && salary<=150000){
			break;
		} else {
			printf("Incorrect input. Please re-enter\n");
		}		
	}
	struct Employee ee;
	ee.six_digit_ID = max_id + 1;
	strcpy(ee.first_name,first_name);
	strcpy(ee.last_name,last_name);
	ee.salary = salary;
	displayList(&ee, 1);
	printf("Confirm the new employee[1:yes/0:no]:");
	read_int(&flag);
	if (flag == 1){
		EeDB[num++]=ee;
		max_id++;
	}
	printf("\n");
};

void search_by_id()
{
	printf("Please enter the employee's id: ");
	int key_id;
	int flag;
	while(1){
		flag = read_int(&key_id);
		if (flag == 0 && key_id>=100000 && key_id<=999999){
			break;
		} else {
			printf("Incorrect input. Please re-enter\n");
		}
	}

	int low=0;
	int high=num;
	int mid;
	while(high>=low){
		mid=(high+low)/2;
		if(EeDB[mid].six_digit_ID>key_id){
			high=mid-1;
		} else if (EeDB[mid].six_digit_ID<key_id){
			low=mid+1;
		} else {
			displayList(&EeDB[mid], 1);
			printf("\n");	
		 	return;
		}
	}
	printf("There is no employee of the surname.\n");
	printf("\n");
	return;	
};

void search_by_last_name()
{
	char lstnm[MAXNAME];
	int flag;
	while(1){
		printf("Please enter the employee last name: ");
		flag = read_string(lstnm);
		if (flag == 0){
			break;
		} else {
			printf("Incorrect input. Please re-enter\n");
		}	
	}
	int i;
	for(i=0; i<=num; i++){
		if(strcmp(lstnm,EeDB[i].last_name)==0){
			displayList(&EeDB[i], 1);
			printf("\n");
		 	return;
		}
	}
	printf("There is no employee of the surname.\n");
	printf("\n");
	return;	
};

void display()
{
	displayList(EeDB, num);
	printf("\n");
};

void save_DB(char *file){
	printf("Saving data, exit when finished...\n");
	FILE *fp = open_file(file, "w");
	if(fp==NULL){
		printf("File cannot open! \n");
		return;
	}
	int i;
	for(i=0; i<=num; i++){
		if(EeDB[i].six_digit_ID >=100000 && EeDB[i].six_digit_ID <=999999){
			fprintf(fp,"%d %s %s %d\n",EeDB[i].six_digit_ID,EeDB[i].first_name,EeDB[i].last_name,EeDB[i].salary);
		}
	}
	close_file(fp);
	printf("goodbye!\n\n");
};




