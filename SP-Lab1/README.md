﻿# C - based employee database

# project structure

 - Main  : program entry
	 - main.c
 - DB_control  : Database control module
	 - DB_control.h
	 - DB_control.c
 - Readfile : File read-Write module
	 - readfile.h
	 - readfile.c
 - Display : Page display module
	 - displaypage.h
	 - displaypage.c

# development environment
Developers are advised to use the following environment to avoid problems with the release
 - Linux
 - gcc version 9.3.0

# Compiling
 - gcc -c main.c -o main.o
 - gcc -c displaypage.c -o displaypage.o
 - gcc -c readfile.c -o readfile.o
 - gcc -c DB_control.c -o DB_control.o
# Link
 - gcc main.o DB_control.o displaypage.o readfile.o -o workerDB  

