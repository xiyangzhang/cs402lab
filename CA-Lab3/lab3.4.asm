                .data 0x10010000
                
my_array:        .space 40              # reserve 40 bytes (10 words) for my_array
                
initial_value:   .word 0x2              #initial_value is a word (32 bit) with the initial value 2(first digit of my CWID)


                .text
                .globl main 

main:           addu $s0, $ra, $0       # save $31 in $16
                lw $t0, initial_value   # load initial_value to register t0
                la $t1, my_array        # $t1(i) <- start address of my_array
                addi $a1, $t1, 40       # $a1(limit) <- end address of my_array
                move $t2, $t0           # j is in $t2
                
Loop:           ble $a1, $t1, Exit      # exit if limit <= i              
                sw $t2, 0($t1)          # my_array[i] = j             
                addi $t2, $t2, 1        # j++ 
                addi $t1, $t1, 4        # calculate address of next element
                j Loop                
                
Exit:
                                        # restore now the return address in $ra and return from main
                addu $ra, $0, $s0       # return address back in $31
                jr $ra                  # return from main