            .data 0x10010000
var1:       .word 0x2                # var1 is a word (32 bit) with the initial value 2(first digit of my CWID)

            .text
            .globl main
main:       addu $s0, $ra, $0       # save $31 in $16
            lw $t0, var1            # load var1 to register t0
            li $a1, 100
            move $t1, $t0           # i is in $t1
            
Loop:       ble $a1, $t1, Exit      # exit if 100 <= i             
            addi $t0, $t0, 1        # var1 = var1 + 1
            addi $t1, $t1, 1        # i++
            j Loop
            
Exit:
            sw $t0, var1            # store the value in registert0 to var1
            li $v0, 1               # print_int
            lw $a0, var1
            syscall 
                                    # restore now the return address in $ra and return from main
            addu $ra, $0, $s0       # return address back in $31
            jr $ra                  # return from main