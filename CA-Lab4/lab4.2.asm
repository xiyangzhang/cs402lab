                .data 0x10000000
                
                .text
                .globl main
main:           addi $sp, $sp, -4
                sw $ra, 4($sp)      # must save $ra since I'll have a call
                jal test            # call 'test' with no parameters
                nop                 # execute this after 'test' returns
                lw $ra, 4($sp)      # restore the return address in $ra
                addi $sp, $sp, 4
                jr $ra              # return from mian
                
                # The procedure 'test' dose not call any ather procedure. Therefore $ra
                # dose not need to be saved . Since 'tesst' uses no registers there is
                # no need to save any registers.
                
test:           nop                 # this is the procedure named 'test'
                jr $ra              # return from this procedure