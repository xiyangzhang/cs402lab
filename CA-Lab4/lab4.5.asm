            .data 0x10000000
msg1:       .asciiz "Please enter an integer number: "   
msg2:       .asciiz "The factorial is："
             
            .text
            .globl main
                
main:       subu $sp, $sp, 4
            sw $ra, 4($sp)          # save $ra in stack
            
inputN:                             # get the 1st user input number
            li $v0, 4               # system call for print_str
            la $a0, msg1            # address of string to print
            syscall
            
            li $v0, 5               # system call for read_int
            syscall                 # the integer placed in $v0 
            addu $t0, $v0, $0       # move the number in $t0
            
            blt $t0, $0, inputN     # if $t0 < 0 then re input
            
            move $a0, $t0
            jal Factorial
            move $t1, $v0
            
            li $v0, 4               # system call for print_str
            la $a0, msg2            # address of string to print
            syscall
            li $v0, 1               # system call for print_int
            move $a0, $t1
            syscall
            
            lw $ra, 4($sp)
            addu $sp, $sp, 4
            jr $ra
            
Factorial:  subu $sp, $sp, 4
            sw $ra, 4($sp)          # save the return address on stack
            
            beqz $a0, terminate     # test for termination
            subu $sp, $sp, 4        # do not terminate yet
            sw $a0, 4($sp)          # save the parameter
            subu $a0, $a0, 1        # will call with a smaller argument
            jal Factorial

            lw $t0, 4($sp)          # the argument I have saved on stack
            mul $v0, $v0, $t0       # do the multiplication
            lw $ra, 8($sp)          # prepare to return
            addu $sp, $sp, 8        # I’ve popped 2 words (an address and
            jr $ra                  # .. an argument)

terminate:  li $v0, 1               # 0! => 1 is the return value
            lw $ra, 4($sp)          # get the return address
            addu $sp, $sp, 4        # adjust the stack pointer
            jr $ra                  # return 