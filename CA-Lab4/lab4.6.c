#include<stdio.h> 

int Ackermann(int x, int y);


int main()
{
	int x;
	int y;
	int r;
    	
	printf("Please enter first positive integer: ");
	scanf("%d",&x);
	while (x < 0)
    {
		printf("error, please re-enter a positive integer:");
		scanf("%d",&x);
	}
	
	printf("Please enter second positive integer: ");
	scanf("%d", &y);
	while (y < 0)
    {
		printf("error, please re-enter a positive integer:");
		scanf("%d", &y);
	}
	
	r = Ackermann(x, y);
	printf("The value of the Ackermann's function is: %d\n",r);
	return 0;
}

int Ackermann(int x, int y)
{
	if(x == 0)
    {
		return y + 1;
	} 
    else if(y == 0)
    {
		return Ackermann(x-1, 1);	
	} 
    else
    {
		return Ackermann(x-1, Ackermann(x, y-1));
	}
}
